var keys = document.querySelectorAll(".key");

keys.forEach(function (key) {
  key.addEventListener("click", function () {
    var keyPressed = key.textContent;

    keys.forEach(function (element) {
      element.style.color = "black";
    });
    key.style.color = "blue";
  });
});
